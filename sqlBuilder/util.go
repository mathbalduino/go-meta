package sqlBuilder

import (
	"gitlab.com/loxe-tools/go-meta"
)

func buildAlias(alias string, m meta.Column) string {
	return alias + "__" + m.Name()
}
