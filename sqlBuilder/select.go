package sqlBuilder

import (
	"fmt"
	"gitlab.com/loxe-tools/go-meta"
	"strings"
)

func Select(columns meta.ColumnSet) *_select {
	return &_select{columns: columns}
}

type _select struct {
	sql     string
	table   meta.Entity
	columns meta.ColumnSet
}

func (s *_select) From(table meta.Entity) *joinOrWhere {
	s.table = table
	tableName := table.Table()
	tableAlias := tableName

	selection := strings.TrimRight(s.buildSelection(tableAlias, "", s.columns), ",")
	s.sql = fmt.Sprintf("SELECT %s \nFROM %s %s ", selection, tableName, tableAlias)
	return &joinOrWhere{selectBuilder: s}
}

func (s *_select) buildSelection(alias, path string, columnSet meta.ColumnSet) string {
	queryColumns := ""

	for _, column := range columnSet {
		if column.IsForeign() {
			foreignColumns := column.Columns()
			if len(foreignColumns) == 0 {
				// TODO: Throw empty foreign column error
				panic("a")
			}

			newAlias := buildAlias(alias, column)
			if path == "" {
				queryColumns += s.buildSelection(newAlias, column.Name(), foreignColumns)
			} else {
				queryColumns += s.buildSelection(newAlias, path+"."+column.Name(), foreignColumns)
			}
			continue
		}

		queryColumns += "\n"
		columnName := column.Name()
		if path == "" {
			queryColumns += fmt.Sprintf("%s.%s as \"%s\",", alias, columnName, columnName)
			continue
		}
		queryColumns += fmt.Sprintf("%s.%s as \"%s.%s\",", alias, columnName, path, columnName)
	}

	return queryColumns
}
