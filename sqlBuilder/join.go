package sqlBuilder

import (
	"fmt"
	"gitlab.com/loxe-tools/go-meta"
)

type join struct {
	selectBuilder *_select
}

func (j *join) Inner(aliasPath ...meta.Column) *joinOrWhere {
	j.selectBuilder.sql += j.buildJoin("INNER", j.selectBuilder.table.Table(), j.selectBuilder.columns, aliasPath...)
	return &joinOrWhere{selectBuilder: j.selectBuilder}
}

func (j *join) buildJoin(joinType string, path string, columns meta.ColumnSet, aliasPath ...meta.Column) string {
	if len(aliasPath) == 0 {
		// TODO: throw missing join table error
		panic("a")
	}

	columnA := aliasPath[0]
	if !columnA.IsForeign() {
		// TODO: throw not using foreign error
		panic("a")
	}

	for _, columnB := range columns {
		if !columnB.IsForeign() {
			continue
		}

		newPath := buildAlias(path, columnB)
		if columnA.Cmp(columnB) {
			if len(aliasPath) == 1 {
				join := fmt.Sprintf("\n%s JOIN %s %s", joinType, columnA.Foreign().Table(), newPath)
				join += fmt.Sprintf(" ON %s.id = %s.%s ", newPath, path, columnA.Name())
				return join
			}

			return j.buildJoin(joinType, newPath, columnB.Columns(), aliasPath[1:]...)
		}

		if len(aliasPath) > 1 {
			j.buildJoin(joinType, newPath, columnB.Columns(), aliasPath...)
		}
	}

	return ""
}
