package main

import (
	"io/ioutil"
	"strings"
)

func filterFiles(directory string) []string {
	files, e := ioutil.ReadDir(directory)
	if e != nil {
		panic(e)
	}

	selectedFiles := make([]string, 0, len(files))
	for _, file := range files {
		if strings.HasSuffix(file.Name(), ".go") && !strings.HasPrefix(file.Name(), "enum") {
			selectedFiles = append(selectedFiles, file.Name())
		}
	}

	return selectedFiles
}
