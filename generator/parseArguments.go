package main

import (
	"flag"
	"fmt"
)

type metaType string

const (
	metaInput  metaType = "INPUT"
	metaEntity          = "ENTITY"
)

func (m metaType) isValid() bool {
	switch m {
	case metaInput, metaEntity:
		return true
	}
	return false
}

type arguments struct {
	metaImport      string
	directory       string
	outputDirectory string
	outputPackage   string
	metaType        metaType
}

func parseArgs() *arguments {
	metaImport := flag.String("meta-import", "gitlab.com/loxe-tools/go-meta", "The import path to use for Meta's")
	directory := flag.String("directory", "./generator/models", "The directory where the original code files exist")
	outputDir := flag.String("output-directory", "./generator/metas", "The output dir to save the Meta's files")
	outputPkg := flag.String("output-package", "metas", "The output package to ue in the new files")
	type_ := flag.String("meta-type", "ENTITY", "Can be 'input' or 'entity'")
	flag.Parse()

	if !metaType(*type_).isValid() {
		panic(fmt.Sprintf("The meta type %s is not supported", *type_))
	}

	return &arguments{
		metaImport:      *metaImport,
		directory:       *directory,
		outputDirectory: *outputDir,
		outputPackage:   *outputPkg,
		metaType:        metaType(*type_),
	}
}
