package main

import (
	"fmt"
	"go/format"
	"os"
	"strings"
)

type newFile struct {
	fileName string
	data     string
}

func metafyDir(args *arguments) {
	filteredFiles := filterFiles(args.directory)

	newFiles := make([]*newFile, len(filteredFiles))
	for i, file := range filteredFiles {
		if args.metaType == metaInput {
			newFiles[i] = &newFile{
				fileName: file,
				data:     metafyInput(file, args),
			}
		}
		if args.metaType == metaEntity {
			newFiles[i] = &newFile{
				fileName: file,
				data:     metafyEntity(file, args),
			}
		}
	}

	for _, newFile := range newFiles {
		e := os.MkdirAll(args.outputDirectory, os.ModePerm)
		if e != nil {
			panic(e)
		}

		splitName := strings.Split(newFile.fileName, ".")
		f, e := os.Create(fmt.Sprintf("%s/%sMeta.go", args.outputDirectory, splitName[0]))
		if e != nil {
			panic(e)
		}

		data := []byte(newFile.data)
		fmtData, e := format.Source(data)
		if e != nil {
			panic(e)
		}

		_, e = f.Write(fmtData)
		if e != nil {
			panic(e)
		}
	}
}
