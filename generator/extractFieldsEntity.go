package main

import (
	"bufio"
	"strings"
)

type entityField struct {
	fieldName     string
	foreignStruct string
	fieldNameDB   string
}

func extractFieldsEntity(reader *bufio.Reader) []*entityField {
	fields := make([]*entityField, 0, 10)
	for {
		line := readLine(reader)

		if line == "}" {
			break
		}

		// Field name
		finalIdx := strings.Index(line, " ")
		fieldName := line[1:finalIdx]

		// Foreign struct
		foreignStruct := ""
		if strings.Index(line, "*") != -1 {
			startIdx := strings.Index(line, "*")
			finalIdx = strings.Index(line[startIdx:], " ") + startIdx
			foreignStruct = line[startIdx+1 : finalIdx]
			isLower := strings.ToLower(string(foreignStruct[0])) == string(foreignStruct[0])
			if isLower || len(strings.Split(foreignStruct, ".")) > 1 {
				foreignStruct = ""
			}
		}

		// Field name DB
		startIdx := strings.Index(line, "\"")
		finalIdx = strings.Index(line, "\"`")
		fieldNameDB := line[startIdx+1 : finalIdx]

		fields = append(fields, &entityField{
			fieldName:     fieldName,
			foreignStruct: foreignStruct,
			fieldNameDB:   fieldNameDB,
		})
	}

	return fields
}
