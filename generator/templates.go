package main

type InputTemplateVars struct {
	PackageName          string
	MetaImport           string
	StructName           string
	PkgPrivateStructName string
	StructFields         []*InputStructFields
}

const inputTemplate = `package {{.PackageName}}

import (meta "{{.MetaImport}}")

type {{.PkgPrivateStructName}}Meta struct {
	{{range .StructFields -}}
		{{.FieldName}} meta.Field
	{{end}}
}

var {{.StructName}}Meta = &{{.PkgPrivateStructName}}Meta{
	{{range .StructFields -}}
		{{.FieldName}}: meta.NewField("{{.FieldNameUncap}}"),
	{{end}}
}

`

type EntityTemplateVars struct {
	PackageName          string
	MetaImport           string
	StructName           string
	TableName            string
	PkgPrivateStructName string
	StructFields         []*EntityStructFields
}

const entityTemplate = `package {{.PackageName}}

import (meta "{{.MetaImport}}")

type {{.PkgPrivateStructName}}Meta struct {
	{{range .StructFields -}}
		{{.FieldName}} meta.Column
	{{end}}
}

func (m *{{.PkgPrivateStructName}}Meta) Struct() string { return "{{.StructName}}" }
func (m *{{.PkgPrivateStructName}}Meta) Table() string { return "{{.TableName}}" }

func (m *{{.PkgPrivateStructName}}Meta) FromField(field string) meta.Column {
	switch field {
		{{- range .StructFields}}
			case m.{{.FieldName}}.FieldName():
				return m.{{.FieldName}}
		{{- end}}
	}

	return nil
}

var {{.StructName}}Meta = func() *{{.PkgPrivateStructName}}Meta {
	m := &{{.PkgPrivateStructName}}Meta{}
	{{range .StructFields -}}
		{{if .FieldForeignStruct -}}
			m.{{.FieldName}} = meta.NewForeignColumn("{{.FieldNameUncap}}", "{{.FieldNameSnake}}", m, {{.FieldForeignStruct}}Meta)
		{{- else -}}
			m.{{.FieldName}} = meta.NewColumn("{{.FieldNameUncap}}", "{{.FieldNameSnake}}", m)
		{{- end}}
	{{end}}

	return m
}()

var {{.StructName}}Meta_all = meta.ColumnSet{
	{{$structName := .StructName -}}
	{{range .StructFields -}}
		{{if .FieldForeignStruct -}}
			{{$structName}}Meta.{{.FieldName}}.With({{.FieldForeignStruct}}Meta_all...),
		{{- else -}}
			{{$structName}}Meta.{{.FieldName}},
		{{- end}}
	{{end}}
}

`
