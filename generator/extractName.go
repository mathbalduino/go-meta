package main

import (
	"bufio"
	"strings"
)

func extractName(reader *bufio.Reader) (string, string) {
	aliasTableName := ""

	for {
		line := readLine(reader)

		if strings.HasPrefix(line, "// Tablename: \"") && strings.HasSuffix(line, "\"") {
			finalIdx := strings.LastIndex(line, "\"")
			aliasTableName = line[15:finalIdx]
		}

		if strings.HasPrefix(line, "type ") && strings.HasSuffix(line, " struct {") {
			finalIdx := strings.Index(line, " struct")
			return line[5:finalIdx], aliasTableName
		}
	}
}
