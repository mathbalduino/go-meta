package meta

type Entity interface {
	Struct() string
	Table() string
	FromField(field string) Column
}
