package meta

type Field interface {
	Name() string
}

func NewField(name string) Field {
	return &field{name: name}
}

type field struct {
	name string
}

func (m *field) Name() string { return m.name }
